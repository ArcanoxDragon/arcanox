﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Net.Wifi;
using Android.Nfc;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Arcanox.App.Models.Event;
using Arcanox.App.Ui;
using Arcanox.Lib.Android.Managers;
using Arcanox.Lib.Android.Utility;
using Arcanox.Lib.Managers;
using Arcanox.Lib.Managers.Abstract;
using Microsoft.Extensions.DependencyInjection;
using Encoding = System.Text.Encoding;

namespace Arcanox.App
{
	[ Activity(
		Label        = "Arcanox",
		MainLauncher = true,
		Icon         = "@drawable/Arcanox",
		Theme        = "@android:style/Theme.Material.Light" ) ]
	public class MainActivity : Activity
	{
		private const string NfcWriteTagExtra = "NfcMessage";

		private ILogManager    logManager;
		private LightsManager  lightsManager;
		private Toast          waitingToast;
		private NfcAdapter     nfcAdapter;
		private PendingIntent  pendingIntent;
		private IntentFilter[] intentFilters;
		private string         tagContentsToWrite;

		private LinearLayout        layout;
		private HueSaturationPicker colorPicker;
		private Button              buttonWrite;

		protected MainActivity( IntPtr javaReference, JniHandleOwnership transfer ) : base( javaReference, transfer )
		{
			Services.Initialize( this.ConfigureServices );
		}

		public MainActivity()
		{
			Services.Initialize( this.ConfigureServices );
		}

		private void ConfigureServices( IServiceCollection services )
		{
			// System services
			services.AddTransient( s => (WifiManager) this.GetSystemService( WifiService ) );
			services.AddTransient( s => (Vibrator) this.GetSystemService( VibratorService ) );
		}

		protected override async void OnCreate( Bundle bundle )
		{
			base.OnCreate( bundle );

			// Local services
			this.lightsManager = Services.Get<LightsManager>();
			this.logManager    = Services.Get<ILogManager>();

			// Set our view from the "main" layout resource
			this.SetContentView( Resource.Layout.Main );

			// Find controls
			this.layout      = this.FindViewById<LinearLayout>( Resource.Id.layout );
			this.colorPicker = this.FindViewById<HueSaturationPicker>( Resource.Id.colorPicker );
			this.buttonWrite = this.FindViewById<Button>( Resource.Id.buttonWrite );

			// Hook up properties/events
			this.colorPicker.ContinuousUpdates =  true;
			this.colorPicker.OnColorPicked     += this.ColorPicker_OnColorPicked;
			this.buttonWrite.Click             += this.ButtonWrite_Click;

			this.PrepareAttachToNfc();

			if ( this.Intent.Action == NfcAdapter.ActionNdefDiscovered )
				await this.HandleNdefIntent( this.Intent );
		}

		protected override void OnResume()
		{
			base.OnResume();
			this.nfcAdapter.EnableForegroundDispatch( this, this.pendingIntent, this.intentFilters, null );
		}

		protected override async void OnNewIntent( Intent intent )
		{
			switch ( intent.Action )
			{
				case NfcAdapter.ActionNdefDiscovered:
					if ( !string.IsNullOrEmpty( this.tagContentsToWrite ) )
						goto case NfcAdapter.ActionTagDiscovered;

					await this.HandleNdefIntent( intent );

					break;
				case NfcAdapter.ActionTagDiscovered:
					this.HandleTagIntent( intent );

					break;
			}

			base.OnNewIntent( intent );
		}

		private async Task HandleNdefIntent( Intent intent )
		{
			var messages = intent.GetParcelableArrayExtra( NfcAdapter.ExtraNdefMessages );
			var message  = messages?.OfType<NdefMessage>().FirstOrDefault();
			var records  = message?.GetRecords();

			if ( records?.Length > 0 )
			{
				var relayRecord = records.First();
				var tagContents = Encoding.ASCII.GetString( relayRecord.GetPayload() );

				Toast.MakeText( this, tagContents, ToastLength.Short );

				var colorMatch = Regex.Match( tagContents, @"color=#([a-f0-9]{6})" );

				if ( colorMatch.Success )
				{
					var colorString = $"FF{colorMatch.Groups[ 1 ].Value}";
					var colorValue  = Convert.ToInt32( colorString, 16 );
					var color       = new Color( colorValue );

					this.colorPicker.CurrentColor = color;
					await this.ChangeColor( color, true );
				}
			}
		}

		private void HandleTagIntent( Intent intent )
		{
			if ( !string.IsNullOrEmpty( this.tagContentsToWrite ) )
			{
				var contentsToWrite = this.tagContentsToWrite;

				this.waitingToast?.Cancel();
				this.tagContentsToWrite = null;

				var tag    = (Tag) intent.GetParcelableExtra( NfcAdapter.ExtraTag );
				var nfc    = Services.Get<NdefManager>();
				var result = nfc.WriteTag( this, tag, contentsToWrite );

				if ( result.Successful )
				{
					var vibrator = Services.Get<Vibrator>();

					vibrator.Vibrate( VibrationEffect.CreateOneShot( 250, VibrationEffect.DefaultAmplitude ) );
					Toast.MakeText( this, "Tag written successfully", ToastLength.Short ).Show();
				}
				else
				{
					DialogHelper.ShowMessage( this,
											  $"Error writing tag: {result.ErrorMessage}",
											  "Tag Write Error" );
				}
			}
		}

		private async void ColorPicker_OnColorPicked( object sender, PickColorEventArgs e )
		{
			await this.ChangeColor( e.ColorPicked, e.WasTouchUp );
		}

		private void ButtonWrite_Click( object sender, EventArgs e )
		{
			var currentColor = this.colorPicker.CurrentColor;
			var colorString  = currentColor.ToArgb().ToString( "x8" ).Substring( 2 );
			var message      = $"color=#{colorString}";

			this.tagContentsToWrite = message;
			this.waitingToast       = Toast.MakeText( this, "Hold the tag to the back of the phone", ToastLength.Long );
			this.waitingToast.Show();
		}

		private async Task ChangeColor( Color color, bool commit )
		{
			if ( commit )
			{
				var colorString = color.ToArgb().ToString( "x8" ).Substring( 2 );

				await this.lightsManager.SetColorOverrideAsync( colorString );
			}

			this.layout.SetBackgroundColor( color );
			this.logManager.Debug( $"Color picked: #{color.ToArgb():x6}" );
		}

		private void PrepareAttachToNfc()
		{
			var nfc                  = NfcAdapter.GetDefaultAdapter( this );
			var intent               = new Intent( this, typeof( MainActivity ) ).AddFlags( ActivityFlags.SingleTop );
			var pendingIntent        = PendingIntent.GetActivity( this, 0, intent, PendingIntentFlags.UpdateCurrent );
			var filterNdefDiscovered = new IntentFilter( NfcAdapter.ActionNdefDiscovered );
			var filterTagDiscovered  = new IntentFilter( NfcAdapter.ActionTagDiscovered );

			filterNdefDiscovered.AddDataType( "x/arcanox" );

			this.nfcAdapter    = nfc;
			this.pendingIntent = pendingIntent;
			this.intentFilters = new[] { filterNdefDiscovered, filterTagDiscovered };
		}
	}
}