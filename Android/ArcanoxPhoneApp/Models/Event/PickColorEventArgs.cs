﻿using System;
using Android.Graphics;

namespace Arcanox.App.Models.Event
{
	public class PickColorEventArgs : EventArgs
	{
		public Color ColorPicked { get; set; }
		public bool  WasTouchUp  { get; set; }
	}
}