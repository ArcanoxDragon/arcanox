using System;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Arcanox.App.Models.Event;
using Arcanox.Lib.Utility;

namespace Arcanox.App.Ui
{
	class HueSaturationPicker : View
	{
		#region Static Constants

		private const float CursorRadius = 32f;

		private static readonly int[] HueStops = new[] {
			Color.Red,
			Color.Yellow,
			Color.Green,
			Color.Cyan,
			Color.Blue,
			Color.Magenta,
			Color.Red
		}.Select( c => c.ToArgb() ).ToArray();

		private static readonly int[] FadeStops = new[] {
			Color.Transparent,
			Color.Black
		}.Select( c => c.ToArgb() ).ToArray();

		#endregion

		public event EventHandler<PickColorEventArgs> OnColorPicked;

		private Color currentColor = Color.Black;
		private float cursorX      = 0f;
		private float cursorY      = 1f;

		private Paint          backgroundPaint;
		private Paint          cursorPaint;
		private LinearGradient hueGradient;
		private LinearGradient fadeGradient;
		private int            lastWidth  = -1;
		private int            lastHeight = -1;

		#region Constructors

		public HueSaturationPicker( Context context ) : base( context ) { }

		public HueSaturationPicker( Context context, IAttributeSet attrs ) : base( context, attrs ) { }

		public HueSaturationPicker( Context context, IAttributeSet attrs, int defStyleAttr ) : base( context, attrs, defStyleAttr ) { }

		public HueSaturationPicker( Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes ) : base( context, attrs, defStyleAttr, defStyleRes ) { }

		protected HueSaturationPicker( IntPtr javaReference, JniHandleOwnership transfer ) : base( javaReference, transfer ) { }

		#endregion Constructors

		#region Properties

		public bool ContinuousUpdates { get; set; }

		public Color CurrentColor
		{
			get => this.currentColor;
			set
			{
				this.currentColor              = value;
				( this.cursorX, this.cursorY ) = this.GetCoordinates( this.currentColor );
				this.Invalidate();
			}
		}

		public float CursorX
		{
			get => this.cursorX;
			set
			{
				this.cursorX      = MathUtil.Clamp( value, 0f, 1f );
				this.currentColor = this.GetColor( this.cursorX, this.cursorY );
				this.Invalidate();
			}
		}

		public float CursorY
		{
			get => this.cursorY;
			set
			{
				this.cursorY      = MathUtil.Clamp( value, 0f, 1f );
				this.currentColor = this.GetColor( this.cursorX, this.cursorY );
				this.Invalidate();
			}
		}

		#endregion

		protected override void OnDraw( Canvas canvas )
		{
			if ( this.backgroundPaint == null )
				this.backgroundPaint = new Paint();

			if ( this.cursorPaint == null )
				this.cursorPaint = new Paint( PaintFlags.AntiAlias );

			// Horizontal hue
			if ( canvas.Width != this.lastWidth )
				this.hueGradient = new LinearGradient( 0, 0, canvas.Width, 0, HueStops, null, Shader.TileMode.Mirror );

			this.backgroundPaint.SetShader( this.hueGradient );
			canvas.DrawPaint( this.backgroundPaint );

			// Vertical fade
			if ( canvas.Height != this.lastHeight )
				this.fadeGradient = new LinearGradient( 0, 0, 0, canvas.Height, FadeStops, null, Shader.TileMode.Mirror );

			this.backgroundPaint.SetShader( this.fadeGradient );
			canvas.DrawPaint( this.backgroundPaint );

			// Cursor
			var ( cursorX, cursorY ) = ( this.CursorX, this.CursorY );

			cursorX *= canvas.Width;
			cursorY *= canvas.Height;

			// Fill
			this.cursorPaint.Color = this.CurrentColor;
			this.cursorPaint.SetStyle( Paint.Style.Fill );
			canvas.DrawCircle( cursorX, cursorY, CursorRadius, this.cursorPaint );

			// Stroke
			this.cursorPaint.StrokeWidth = 4;
			this.cursorPaint.Color       = Color.Black;
			this.cursorPaint.SetStyle( Paint.Style.Stroke );
			canvas.DrawCircle( cursorX, cursorY, CursorRadius, this.cursorPaint );

			this.lastWidth  = canvas.Width;
			this.lastHeight = canvas.Height;
		}

		public override bool OnTouchEvent( MotionEvent e )
		{
			switch ( e.Action )
			{
				case MotionEventActions.Down:
				case MotionEventActions.Move:
					var cursorX = e.GetX();
					var cursorY = e.GetY();

					cursorX /= this.Width;
					cursorY /= this.Height;

					this.CursorX      = cursorX;
					this.CursorY      = cursorY;
					this.CurrentColor = this.GetColor( cursorX, cursorY );

					if ( this.ContinuousUpdates )
						this.OnColorPicked?.Invoke( this, new PickColorEventArgs {
							ColorPicked = this.CurrentColor
						} );

					return true;
				case MotionEventActions.Up:
					this.OnColorPicked?.Invoke( this, new PickColorEventArgs {
						ColorPicked = this.CurrentColor,
						WasTouchUp  = true
					} );
					return true;
				default:
					return base.OnTouchEvent( e );
			}
		}

		protected Color GetColor( float x, float y )
		{
			var hue        = x * 360f;
			var brightness = 1f - y;

			return Color.HSVToColor( new[] { hue, 1f, brightness } );
		}

		protected (float x, float y) GetCoordinates( Color color )
		{
			var hue        = color.GetHue();
			var brightness = color.GetBrightness();

			return ( hue / 360f, 1f - brightness * 2f );
		}

		protected override void OnMeasure( int widthMeasureSpec, int heightMeasureSpec )
		{
			var minDimension = Math.Min( widthMeasureSpec, heightMeasureSpec );

			base.OnMeasure( minDimension, minDimension );
		}
	}
}