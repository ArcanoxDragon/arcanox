﻿using System;
using System.Threading.Tasks;
using Android.Animation;
using Android.App;
using Android.Net.Wifi;
using Android.OS;
using Android.Runtime;
using Android.Support.Wearable.Activity;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Arcanox.Lib.Android.Extensions;
using Arcanox.Lib.Android.Utility;
using Arcanox.Lib.Extensions;
using Arcanox.Lib.Managers;
using Arcanox.Lib.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.WearApp
{
	[ Activity( Label = "@string/app_name", MainLauncher = true, Icon = "@drawable/icon" ) ]
	public class MainActivity : WearableActivity
	{
		// Misc. fields
		private LightsManager manager;
		private LightsConfig  config = new LightsConfig();

		// Views
		private ImageButton buttonOn;
		private ImageButton buttonOff;
		private ImageButton buttonDecBrightness;
		private ImageButton buttonIncBrightness;
		private TextView    textViewBrightness;
		private TextView    textViewAmbient;

		public MainActivity( IntPtr javaReference, JniHandleOwnership transfer ) : base( javaReference, transfer )
		{
			Services.Initialize( this.ConfigureServices );
		}

		public MainActivity()
		{
			Services.Initialize( this.ConfigureServices );
		}

		private void ConfigureServices( IServiceCollection services )
		{
			// System services
			services.AddTransient( s => (WifiManager) this.GetSystemService( WifiService ) );
		}

		protected override async void OnCreate( Bundle bundle )
		{
			base.OnCreate( bundle );

			// Initialize local services
			this.manager = Services.Get<LightsManager>();

			this.SetContentView( Resource.Layout.activity_main );

			// Retrieve handles to views
			this.buttonOn            = this.FindViewById<ImageButton>( Resource.Id.buttonOn );
			this.buttonOff           = this.FindViewById<ImageButton>( Resource.Id.buttonOff );
			this.buttonDecBrightness = this.FindViewById<ImageButton>( Resource.Id.buttonDecBrightness );
			this.buttonIncBrightness = this.FindViewById<ImageButton>( Resource.Id.buttonIncBrightness );
			this.textViewBrightness  = this.FindViewById<TextView>( Resource.Id.textViewBrightness );
			this.textViewAmbient     = this.FindViewById<TextView>( Resource.Id.textViewAmbient );

			// Hook up events
			this.buttonOn.Click            += this.ButtonOn_Click;
			this.buttonOff.Click           += this.ButtonOff_Click;
			this.buttonIncBrightness.Click += this.ButtonIncBrightness_Click;
			this.buttonDecBrightness.Click += this.ButtonDecBrightness_Click;

			await this.UpdateAsync();
			this.SetAmbientEnabled();

			// Start update loop
			this.UpdateLoop().ConfigureAwait( false );
		}

		public override bool OnKeyDown( Keycode keyCode, KeyEvent e )
		{
			switch ( keyCode )
			{
				case Keycode.NavigateNext:
					// Lights On

					this.ButtonOn_Click( this, null );
					return true;
				case Keycode.NavigatePrevious:
					// Lights Off

					this.ButtonOff_Click( this, null );
					return true;
				default:
					return base.OnKeyDown( keyCode, e );
			}
		}

		public override void OnEnterAmbient( Bundle ambientDetails )
		{
			var fadeOut = new AlphaAnimation( 1f, 0f ) {
				Interpolator = new AccelerateInterpolator(),
				Duration     = 350
			};
			var fadeIn = new AlphaAnimation( 0f, 1f ) {
				Interpolator = new AccelerateInterpolator(),
				Duration     = 350
			};

			fadeOut.AnimationEnd += ( s, e ) => {
				this.buttonOn.Visibility            = ViewStates.Invisible;
				this.buttonOff.Visibility           = ViewStates.Invisible;
				this.buttonDecBrightness.Visibility = ViewStates.Invisible;
				this.buttonIncBrightness.Visibility = ViewStates.Invisible;
			};

			fadeIn.AnimationStart += ( s, e )
				=> this.textViewAmbient.Visibility = ViewStates.Visible;

			this.buttonOn.StartAnimation( fadeOut );
			this.buttonOff.StartAnimation( fadeOut );
			this.buttonDecBrightness.StartAnimation( fadeOut );
			this.buttonIncBrightness.StartAnimation( fadeOut );
			this.textViewBrightness.StartAnimation( fadeOut );
			this.textViewAmbient.StartAnimation( fadeIn );
		}

		public override void OnExitAmbient()
		{
			var fadeOut = new AlphaAnimation( 1f, 0f ) {
				Interpolator = new AccelerateInterpolator(),
				Duration     = 350
			};
			var fadeIn = new AlphaAnimation( 0f, 1f ) {
				Interpolator = new AccelerateInterpolator(),
				Duration     = 350
			};

			fadeOut.AnimationEnd += ( s, e ) 
				=> this.textViewAmbient.Visibility = ViewStates.Invisible;

			fadeIn.AnimationStart += ( s, e ) => {
				this.buttonOn.Visibility            = ViewStates.Visible;
				this.buttonOff.Visibility           = ViewStates.Visible;
				this.buttonDecBrightness.Visibility = ViewStates.Visible;
				this.buttonIncBrightness.Visibility = ViewStates.Visible;
			};

			this.buttonOn.StartAnimation( fadeIn );
			this.buttonOff.StartAnimation( fadeIn );
			this.buttonDecBrightness.StartAnimation( fadeIn );
			this.buttonIncBrightness.StartAnimation( fadeIn );
			this.textViewBrightness.StartAnimation( fadeIn );
			this.textViewAmbient.StartAnimation( fadeOut );
		}

		private async Task UpdateLoop()
		{
			while ( true )
			{
				await this.UpdateAsync();
				await Task.Delay( 5000 );
			}
		}

		private async Task DoWork( Func<Task> workAction )
		{
			// Temporarily disable UI
			this.buttonOn.Enabled            = false;
			this.buttonOff.Enabled           = false;
			this.buttonIncBrightness.Enabled = false;
			this.buttonDecBrightness.Enabled = false;

			await workAction();

			// Re-enable UI
			this.buttonOn.Enabled            = true;
			this.buttonOff.Enabled           = true;
			this.buttonIncBrightness.Enabled = true;
			this.buttonDecBrightness.Enabled = true;
		}

		private void UpdateFaceText( bool on, double brightness )
		{
			var timeText       = DateTime.Now.ToString( "hh:mm tt" ).ToLower();
			var brightnessText = on ? $"{brightness:0%}" : "Off";

			this.textViewBrightness.Text = $"{timeText}\n{brightnessText}";
			this.textViewAmbient.Text    = $"{timeText}\n{brightnessText}";
		}

		private async Task UpdateAsync()
		{
			this.config = await this.manager.GetConfig() ?? new LightsConfig();
			this.UpdateFaceText( this.config.On, this.config.Brightness );
		}

		private async void ButtonOn_Click( object sender, EventArgs e )
		{
			await this.DoWork( async () => {
				this.UpdateFaceText( true, this.config.Brightness );

				await this.manager.SetOnAsync( true );
				await this.UpdateAsync();
			} );
		}

		private async void ButtonOff_Click( object sender, EventArgs e )
		{
			await this.DoWork( async () => {
				this.UpdateFaceText( false, 0.0 );

				await this.manager.SetOnAsync( false );
				await this.UpdateAsync();
			} );
		}

		private async void ButtonIncBrightness_Click( object sender, EventArgs e )
		{
			await this.DoWork( async () => {
				this.UpdateFaceText( this.config.On, Math.Min( this.config.Brightness + 0.1, 1.0 ) );

				await this.manager.IncreaseBrightnessAsync();
				await this.UpdateAsync();
			} );
		}

		private async void ButtonDecBrightness_Click( object sender, EventArgs e )
		{
			await this.DoWork( async () => {
				this.UpdateFaceText( this.config.On, Math.Max( this.config.Brightness - 0.1, 0.1 ) );

				await this.manager.DecreaseBrightnessAsync();
				await this.UpdateAsync();
			} );
		}
	}
}