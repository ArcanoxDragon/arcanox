﻿using Android.App;
using Android.Content;

namespace Arcanox.Lib.Android.Utility
{
	public static class DialogHelper
	{
		public static void ShowMessage( Context context, string message, string title )
		{
			var builder = new AlertDialog.Builder( context );

			builder.SetTitle( title )
				   .SetMessage( message )
				   .SetPositiveButton( "OK", ( s, e ) => { } )
				   .Show();
		}
	}
}