﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using Android.Util;
using Arcanox.Lib.Android.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Arcanox.Lib.Extensions;

namespace Arcanox.Lib.Android.Utility
{
	public class Services : IServiceProvider
	{
		#region Static

		public static Services Instance { get; private set; }

		public static void Initialize( Action<IServiceCollection> configureAction )
		{
			Instance = new Services( configureAction );
		}

		public static TService Get<TService>() => Instance.GetRequiredService<TService>();

		public static TInstance New<TInstance>( params object[] parameters )
			=> Instance.CreateInstance<TInstance>( parameters );

		#endregion

		private readonly Action<IServiceCollection> configureAction;
		private readonly IServiceProvider           serviceProvider;

		private Services( Action<IServiceCollection> configureAction )
		{
			this.configureAction = configureAction;
			this.serviceProvider = this.ConfigureServices();
		}

		private IServiceProvider ConfigureServices()
		{
			var services = new ServiceCollection();

			// Configure library services
			Log.Info( Constants.LogTag, "Registering Arcanox services..." );
			services.AddArcanox();
			services.AddArcanoxAndroid();

			// Configure application services
			this.configureAction( services );

			return services.BuildServiceProvider();
		}

		public object GetService( Type serviceType )
		{
			Log.Info( Constants.LogTag, $"Getting service of type {serviceType.FullName}" );

			var engine      = this.serviceProvider.GetType().GetField( "_engine" )?.GetValue( this.serviceProvider );
			var collections = engine?.GetType().GetProperty( "RealizedServices" )?.GetValue( engine );

			Log.Info( Constants.LogTag, $"engine is {engine?.GetType().FullName} | {engine}" );
			Log.Info( Constants.LogTag, $"collections is {engine?.GetType().FullName} | {engine}" );

			if ( collections is IDictionary dictionary )
			{
				foreach ( var key in dictionary.Keys )
				{
					var type = (Type) key;
					Log.Info( Constants.LogTag, $"Found registered service: {type.FullName}" );
				}
			}

			return this.serviceProvider.GetService( serviceType );
		}
	}
}