﻿using System;
using Android.Util;
using Arcanox.Lib.Android.Utility;
using Arcanox.Lib.Managers.Abstract;

namespace Arcanox.Lib.Android.Managers
{
	public class LogManager : ILogManager
	{
		public void Info( string message )
			=> this.WriteLine( message, LogPriority.Info );

		public void Debug( string message )
			=> this.WriteLine( message, LogPriority.Debug );

		public void Error( string message )
			=> this.WriteLine( message, LogPriority.Error );

		private void WriteLine( string message, LogPriority priority )
		{
			Console.WriteLine( $"[{Constants.LogTag}] {message}" );
			Log.WriteLine( priority, Constants.LogTag, message );
		}
	}
}