﻿using Arcanox.Lib.Managers.Abstract;

namespace Arcanox.Lib.Android.Managers
{
    public class ArcanoxManager : IArcanoxManager
    {
		private const string BaseUri     = "https://fursuit.arcanox.me";
		private const string BaseUriHome = "http://10.0.0.16:51866";

		private readonly NetworkManager networkManager;

		public ArcanoxManager(NetworkManager networkManager)
		{
			this.networkManager = networkManager;
		}

		public string GetApiUrl() => this.networkManager.IsAtHome() ? BaseUriHome : BaseUri;
	}
}
