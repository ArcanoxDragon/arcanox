﻿using System.Linq;
using System.Text.RegularExpressions;
using Android.Net.Wifi;

namespace Arcanox.Lib.Android.Managers
{
	public class NetworkManager
	{
		private static readonly string[] HomeNetworkNames = {
			"The Furry Side Of The Fence",
			"The Furry 5ide Of The Fence"
		};

		private readonly WifiManager wifi;

		public NetworkManager( WifiManager wifi )
		{
			this.wifi = wifi;
		}

		public bool IsAtHome()
		{
			var currentNetwork = this.wifi.ConnectionInfo.SSID;
			var networkMatch   = Regex.Match( currentNetwork, "\"([^\"]+)\"" );

			if ( !networkMatch.Success )
				return false;

			var networkName = networkMatch.Groups[ 1 ].Value;

			return HomeNetworkNames.Contains( networkName );
		}
	}
}