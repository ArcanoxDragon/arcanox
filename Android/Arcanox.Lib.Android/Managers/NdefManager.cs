﻿using System;
using System.Text;
using Android.Content;
using Android.Nfc;
using Android.Nfc.Tech;
using Arcanox.Lib.Models;
using Java.IO;
using FormatException = Android.Nfc.FormatException;

namespace Arcanox.Lib.Android.Managers
{
	public class NdefManager
	{
		public FunctionResult WriteTag( Context context, Tag tag, string content )
		{
			var appRecord = NdefRecord.CreateApplicationRecord( context.PackageName );
			var relayRecord = new NdefRecord( NdefRecord.TnfMimeMedia,
											  Encoding.ASCII.GetBytes( "x/arcanox" ),
											  null,
											  Encoding.ASCII.GetBytes( content ) );
			var message = new NdefMessage( new[] { relayRecord, appRecord } );

			try
			{
				var existingNdef = Ndef.Get( tag );

				if ( existingNdef == null )
				{
					var format = NdefFormatable.Get( tag );

					if ( format == null )
						return FunctionResult.Fail( "Tag cannot handle NDEF messages" );

					try
					{
						format.Connect();
						format.Format( message );
					}
					catch ( TagLostException ex )
					{
						return FunctionResult.Fail( "Tag was lost while writing" );
					}
					catch ( Exception ex ) when ( ex is IOException || ex is FormatException )
					{
						return FunctionResult.Fail( "A tag format error occurred" );
					}
				}
				else
				{
					// Write to existing record

					existingNdef.Connect();

					if ( !existingNdef.IsWritable )
						return FunctionResult.Fail( "Tag is not writeable" );

					var messageSize = message.ToByteArray().Length;

					if ( messageSize > existingNdef.MaxSize )
						return FunctionResult.Fail( "Contents too big for tag" );

					try
					{
						existingNdef.WriteNdefMessage( message );
					}
					catch ( TagLostException ex )
					{
						return FunctionResult.Fail( "Tag was lost while writing" );
					}
					catch ( Exception ex ) when ( ex is IOException || ex is FormatException )
					{
						return FunctionResult.Fail( "A tag format error occurred" );
					}
				}

				return FunctionResult.Success();
			}
			catch ( Exception ex )
			{
				return FunctionResult.Fail( $"Unknown NFC error: {ex.Message}" );
			}
		}
	}
}