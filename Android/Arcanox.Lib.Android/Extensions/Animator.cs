﻿using Android.Animation;

namespace Arcanox.Lib.Android.Extensions
{
    public static class AnimatorExtensions
    {
		public static void TryCancel( this Animator animator )
		{
			try
			{
				animator.Cancel();
			}
			catch
			{
				// ignored
			}
		}
    }
}
