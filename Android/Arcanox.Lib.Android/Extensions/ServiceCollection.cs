﻿using Arcanox.Lib.Android.Managers;
using Arcanox.Lib.Managers.Abstract;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Arcanox.Lib.Android.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static void AddArcanoxAndroid( this IServiceCollection services )
		{
			services.TryAddTransient<ILogManager, LogManager>();
			services.TryAddTransient<IArcanoxManager, ArcanoxManager>();
			services.TryAddTransient<NetworkManager>();
			services.TryAddTransient<NdefManager>();
		}
	}
}