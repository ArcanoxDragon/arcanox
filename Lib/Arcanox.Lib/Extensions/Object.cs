﻿using System;

namespace Arcanox.Lib.Extensions
{
    public static class ObjectExtensions
    {
		public static TObj With<TObj>( this TObj obj, Action<TObj> action )
		{
			action( obj );
			return obj;
		}
    }
}
