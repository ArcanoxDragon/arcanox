﻿using Arcanox.Lib.Managers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Arcanox.Lib.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static void AddArcanox( this IServiceCollection services )
		{
			services.TryAddTransient<LightsManager>();
		}
	}
}