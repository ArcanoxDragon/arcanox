﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.Lib.Extensions
{
	public static class ServiceProviderExtensions
	{
		public static TService CreateInstance<TService>( this IServiceProvider services, params object[] parameters )
			=> ActivatorUtilities.CreateInstance<TService>( services, parameters );
	}
}