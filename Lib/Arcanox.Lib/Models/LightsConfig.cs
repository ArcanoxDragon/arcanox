﻿namespace Arcanox.Lib.Models
{
	public class LightsConfig
	{
		public double Brightness    { get; set; }
		public bool   On            { get; set; }
		public string ColorOverride { get; set; }
	}
}