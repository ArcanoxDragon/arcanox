﻿namespace Arcanox.Lib.Models
{
	public class FunctionResult
	{
		public static FunctionResult Success()
			=> new FunctionResult { Successful = true };

		public static FunctionResult Fail( string errorMessage = "" )
			=> new FunctionResult { Successful = false, ErrorMessage = errorMessage };

		public bool   Successful   { get; set; }
		public string ErrorMessage { get; set; }
	}
}