﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arcanox.Lib.Utility
{
	public class SingleThreadTaskScheduler : TaskScheduler
	{
		private readonly BlockingCollection<Task> scheduledTasks;
		private readonly Thread                   workerThread;

		public SingleThreadTaskScheduler()
		{
			this.scheduledTasks = new BlockingCollection<Task>();
			this.workerThread   = new Thread( this.Execute );

			if ( !this.workerThread.IsAlive )
				this.workerThread.Start();
		}

		private void Execute()
		{
			foreach ( var task in this.scheduledTasks.GetConsumingEnumerable() )
			{
				this.TryExecuteTask( task );
			}
		}

		protected override void QueueTask( Task task ) => this.scheduledTasks.Add( task );

		protected override bool TryExecuteTaskInline( Task task, bool taskWasPreviouslyQueued ) => false;

		protected override IEnumerable<Task> GetScheduledTasks() => this.scheduledTasks.ToArray();

		private void Dispose( bool disposing )

		{
			if ( !disposing ) return;

			this.scheduledTasks.CompleteAdding();
			this.scheduledTasks.Dispose();
		}

		public void Dispose()

		{
			this.Dispose( true );
			GC.SuppressFinalize( this );
		}
	}
}