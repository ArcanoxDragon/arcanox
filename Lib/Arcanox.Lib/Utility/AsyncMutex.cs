﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Arcanox.Lib.Utility
{
	public class AsyncMutex
	{
		private static readonly TaskScheduler SingleThreadTaskScheduler = new SingleThreadTaskScheduler();

		private readonly Mutex mutex;

		public AsyncMutex()
		{
			this.mutex = new Mutex();
		}

		private async Task<TResult> QueueMutexCall<TResult>( Func<TResult> callFunc )
			=> await Task.Factory.StartNew( callFunc,
											CancellationToken.None,
											TaskCreationOptions.None,
											SingleThreadTaskScheduler );

		private async Task QueueMutexCall( Action callAction )
			=> await Task.Factory.StartNew( callAction,
											CancellationToken.None,
											TaskCreationOptions.None,
											SingleThreadTaskScheduler );

		public Task<bool> WaitOneAsync()
			=> this.QueueMutexCall( () => this.mutex.WaitOne() );

		public Task<bool> WaitOneAsync( TimeSpan timeout )
			=> this.QueueMutexCall( () => this.mutex.WaitOne( timeout ) );

		public Task<bool> WaitOneAsync( TimeSpan timeout, bool exitContext )
			=> this.QueueMutexCall( () => this.mutex.WaitOne( timeout, exitContext ) );

		public Task<bool> WaitOneAsync( int millisecondsTimeout )
			=> this.QueueMutexCall( () => this.mutex.WaitOne( millisecondsTimeout ) );

		public Task<bool> WaitOneAsync( int millisecondsTimeout, bool exitContext )
			=> this.QueueMutexCall( () => this.mutex.WaitOne( millisecondsTimeout, exitContext ) );

		public Task ReleaseMutexAsync()
			=> this.QueueMutexCall( this.mutex.ReleaseMutex );
	}
}