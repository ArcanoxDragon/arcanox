﻿using System;

namespace Arcanox.Lib.Utility
{
	public static class MathUtil
	{
		public static float Clamp( float x, float min, float max )
			=> Math.Min( Math.Max( x, min ), max );

		public static double Clamp( double x, double min, double max )
			=> Math.Min( Math.Max( x, min ), max );

		public static int Clamp( int x, int min, int max )
			=> Math.Min( Math.Max( x, min ), max );
	}
}