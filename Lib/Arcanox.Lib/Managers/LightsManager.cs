﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Arcanox.Lib.Managers.Abstract;
using Arcanox.Lib.Models;
using Arcanox.Lib.Utility;
using Newtonsoft.Json;

namespace Arcanox.Lib.Managers
{
	[ SuppressMessage( "ReSharper", "ArrangeThisQualifier" ) ]
	public class LightsManager : IDisposable
	{
		private readonly IArcanoxManager arcanoxManager;
		private readonly ILogManager     logManager;
		private readonly AsyncMutex      workMutex;
		private readonly HttpClient      http;

		public LightsManager( IArcanoxManager arcanoxManager,
							  ILogManager logManager )
		{
			this.arcanoxManager = arcanoxManager;
			this.logManager     = logManager;
			this.workMutex      = new AsyncMutex();
			this.http           = new HttpClient { Timeout = TimeSpan.FromSeconds( 3 ) };
		}

		protected string ApiUrl => this.arcanoxManager.GetApiUrl();

		private async Task DoWork( Func<Task> workFunc )
		{
			if ( !await this.workMutex.WaitOneAsync( 3000 ) )
				return;

			try
			{
				await workFunc();
			}
			catch ( Exception e )
			{
				this.logManager.Error( "Error:" );
				this.logManager.Error( e.ToString() );
			}
			finally
			{
				await this.workMutex.ReleaseMutexAsync();
			}
		}

		private async Task<TResult> DoWork<TResult>( Func<Task<TResult>> workFunc )
		{
			if ( !await this.workMutex.WaitOneAsync( 3000 ) )
				return default;

			try
			{
				return await workFunc();
			}
			catch ( Exception e )
			{
				this.logManager.Error( "Error:" );
				this.logManager.Error( e.ToString() );

				return default;
			}
			finally
			{
				await this.workMutex.ReleaseMutexAsync();
			}
		}

		public async Task<LightsConfig> GetConfig()
		{
			return await this.DoWork( async () => {
				var response = await this.http.GetAsync( $"{ApiUrl}/api/config" );

				if ( !response.IsSuccessStatusCode )
					return default;

				var content = await response.Content.ReadAsStringAsync();

				return JsonConvert.DeserializeObject<LightsConfig>( content );
			} );
		}

		public async Task IncreaseBrightnessAsync()
		{
			await this.DoWork( async () => {
				await this.http.PostAsync( $"{ApiUrl}/api/increasebrightness", new ByteArrayContent( new byte[ 0 ] ) );
			} );
		}

		public async Task DecreaseBrightnessAsync()
		{
			await this.DoWork( async () => {
				await this.http.PostAsync( $"{ApiUrl}/api/decreasebrightness", new ByteArrayContent( new byte[ 0 ] ) );
			} );
		}

		public async Task SetOnAsync( bool on )
		{
			await this.DoWork( async () => {
				await this.http.PostAsync( $"{ApiUrl}/api/seton?on={( on ? "1" : "0" )}", new ByteArrayContent( new byte[ 0 ] ) );
			} );
		}

		public async Task SetColorOverrideAsync( string colorOverride )
		{
			await this.DoWork( async () => {
				var match = Regex.Match( colorOverride, @"[a-f0-9]{6}", RegexOptions.IgnoreCase );

				if ( !match.Success )
					return;

				var res = await this.http.PostAsync( $"{ApiUrl}/api/setoverride?color={colorOverride}", new ByteArrayContent( new byte[ 0 ] ) );

				
			} );
		}

		public async Task ClearColorOverrideAsync()
		{
			await this.DoWork( async () => {
				await this.http.PostAsync( $"{ApiUrl}/api/clearoverride", new ByteArrayContent( new byte[ 0 ] ) );
			} );
		}

		public void Dispose()
		{
			this.http?.Dispose();
		}
	}
}