﻿namespace Arcanox.Lib.Managers.Abstract
{
	public interface ILogManager
	{
		void Info( string message );
		void Debug( string message );
		void Error( string message );
	}
}