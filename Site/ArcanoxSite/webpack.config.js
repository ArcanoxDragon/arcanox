"use strict";
var glob = require("glob-all");
var path = require("path");
var entryBundles = {};
var entryFiles = glob.sync([
    "./Scripts/Pages/**/*.ts"
], { cwd: __dirname });
entryFiles.forEach(function (entry) {
    var nameMatch = entry.match(/(Pages\/.*?)\.ts$/);
    var bundleName = nameMatch[1];
    entryBundles[bundleName] = entry;
});
console.log(entryBundles);
module.exports = {
    mode: "development",
    entry: entryBundles,
    devtool: "source-map",
    output: {
        path: path.resolve("wwwroot/js"),
        filename: "[name].js"
    },
    resolve: {
        modules: [
            "node_modules",
            "Scripts",
            "Styles"
        ]
    },
    module: {
        rules: [
            {
                test: /\.ts$/i,
                use: "babel-loader"
            },
            {
                test: /\.scss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    {
                        loader: "sass-loader",
                        options: {
                            includePaths: [path.resolve("Styles")]
                        }
                    }
                ]
            }
        ]
    }
};
