﻿namespace ArcanoxSite.Models.Abstract
{
    public interface ILightsConfig
    {
        double Brightness { get; set; }
		bool On { get; set; }
		string ColorOverride { get; set; }
    }
}
