﻿using System.ComponentModel;
using ArcanoxSite.Models.Abstract;

namespace ArcanoxSite.Models.Home
{
	public class IndexViewModel : ILightsConfig
	{
		public double Brightness { get; set; }
		public bool   On         { get; set; }

		[ DisplayName( "Color override (leave blank for none)" ) ] 
		public string ColorOverride { get; set; }
	}
}