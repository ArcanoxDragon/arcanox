﻿using System;
using ArcanoxSite.Models.Abstract;

namespace ArcanoxSite.Services
{
	public class LightsService : ILightsConfig
	{
		private double brightness;
		private bool   on;
		private string colorOverride;

		public LightsService()
		{
			this.brightness    = 1.0;
			this.on            = true;
			this.colorOverride = null;
		}

		public event EventHandler PropertyChanged;

		public double Brightness
		{
			get => this.brightness;
			set
			{
				this.brightness = value;
				this.PropertyChanged?.Invoke( this, new EventArgs() );
			}
		}

		public bool On
		{
			get => this.on;
			set
			{
				this.on = value;
				this.PropertyChanged?.Invoke( this, new EventArgs() );
			}
		}

		public string ColorOverride
		{
			get => this.colorOverride;
			set
			{
				this.colorOverride = value;
				this.PropertyChanged?.Invoke( this, new EventArgs() );
			}
		}

		public void IncreaseBrightness()
		{
			var brightness = this.Brightness;

			brightness += 0.1;

			if ( brightness > 1.0 )
				brightness = 1.0;

			this.Brightness = brightness;
		}

		public void DecreaseBrightness()
		{
			var brightness = this.Brightness;

			brightness -= 0.1;

			if ( brightness < 0.1 )
				brightness = 0.1;

			this.Brightness = brightness;
		}
	}
}