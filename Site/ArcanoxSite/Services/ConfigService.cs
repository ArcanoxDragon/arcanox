﻿using System;
using System.Collections.Generic;
using System.Net;

namespace ArcanoxSite.Services
{
	public class ConfigService
	{
		private readonly Dictionary<IPAddress, DateTime> lastChecks;

		private DateTime lastUpdate;

		public ConfigService( LightsService lightsService )
		{
			this.lastChecks = new Dictionary<IPAddress, DateTime>();
			this.lastUpdate = DateTime.UtcNow;

			lightsService.PropertyChanged += this.OnLightsPropertyChanged;
		}

		private void OnLightsPropertyChanged( object sender, EventArgs e )
		{
			this.lastUpdate = DateTime.UtcNow;
		}

		public bool CheckUpdate( IPAddress ipAddress )
		{
			if ( !this.lastChecks.TryGetValue( ipAddress, out var lastCheck ) )
			{
				this.lastChecks[ ipAddress ] = DateTime.UtcNow;
				return true;
			}

			var needsUpdate = lastCheck <= this.lastUpdate;

			this.lastChecks[ ipAddress ] = DateTime.UtcNow;

			return needsUpdate;
		}
	}
}