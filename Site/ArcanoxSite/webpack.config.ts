import * as glob from "glob-all";
import * as path from "path";
import * as webpack from "webpack";

const entryBundles: { [ key: string ]: string } = {};
const entryFiles: string[] = glob.sync( [
    "./Scripts/Pages/**/*.ts"
], { cwd: __dirname } );

entryFiles.forEach( entry => {
    let nameMatch = entry.match( /(Pages\/.*?)\.ts$/ );
    let bundleName = nameMatch[ 1 ];

    entryBundles[ bundleName ] = entry;
} );

console.log( entryBundles );

export = {
    mode: "development",

    entry: entryBundles,
    devtool: "source-map",

    output: {
        path: path.resolve( "wwwroot/js" ),
        filename: "[name].js"
    },

    resolve: {
        modules: [
            "node_modules",
            "Scripts",
            "Styles"
        ]
    },

    module: {
        rules: [
            {
                test: /\.ts$/i,
                use: "babel-loader"
            },
            {
                test: /\.scss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    {
                        loader: "sass-loader",
                        options: {
                            includePaths: [ path.resolve( "Styles" ) ]
                        }
                    }
                ]
            }
        ]
    }
} as webpack.Configuration;