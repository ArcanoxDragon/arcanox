﻿using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using ArcanoxSite.Models.Home;
using ArcanoxSite.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArcanoxSite.Controllers
{
	[ SuppressMessage( "ReSharper", "ArrangeThisQualifier" ) ]
	public class HomeController : Controller
	{
		private readonly LightsService lightsService;

		public HomeController( LightsService lightsService )
		{
			this.lightsService = lightsService;
		}

		public IActionResult Index()
		{
			var model = new IndexViewModel {
				Brightness    = this.lightsService.Brightness * 100,
				On            = this.lightsService.On,
				ColorOverride = this.lightsService.ColorOverride
			};

			return View( model );
		}

		public IActionResult DecreaseBrightness()
		{
			this.lightsService.DecreaseBrightness();

			return RedirectToAction( "Index" );
		}

		public IActionResult IncreaseBrightness()
		{
			this.lightsService.IncreaseBrightness();

			return RedirectToAction( "Index" );
		}

		public IActionResult ToggleLights()
		{
			this.lightsService.On = !this.lightsService.On;

			return RedirectToAction( "Index" );
		}

		[ HttpPost ]
		public IActionResult SetOverride( IndexViewModel model )
		{
			var colorOverride = default( string );

			if ( !string.IsNullOrEmpty( model?.ColorOverride ) )
			{
				var match = Regex.Match( model.ColorOverride, @"#[a-f0-9]{6}", RegexOptions.IgnoreCase );

				if ( match.Success )
					colorOverride = model.ColorOverride;
			}

			this.lightsService.ColorOverride = colorOverride;

			return RedirectToAction( "Index" );
		}

		[ HttpPost ]
		public IActionResult ClearOverride()
		{
			this.lightsService.ColorOverride = null;

			return RedirectToAction( "Index" );
		}
	}
}