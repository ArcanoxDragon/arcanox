﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ArcanoxSite.Models.Abstract;
using ArcanoxSite.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArcanoxSite.Controllers
{
	[ SuppressMessage( "ReSharper", "ArrangeThisQualifier" ) ]
	public class ApiController : Controller
	{
		private readonly ConfigService configService;
		private readonly LightsService lightsService;

		private TaskCompletionSource<ILightsConfig> pollConfigSource;

		public ApiController( ConfigService configService,
							  LightsService lightsService )
		{
			this.configService                 =  configService;
			this.lightsService                 =  lightsService;
			this.lightsService.PropertyChanged += LightsService_PropertyChanged;
			this.pollConfigSource              =  new TaskCompletionSource<ILightsConfig>();
		}

		private void LightsService_PropertyChanged( object sender, EventArgs e )
		{
			this.pollConfigSource?.SetResult( this.lightsService );
			this.pollConfigSource = new TaskCompletionSource<ILightsConfig>();
		}

		public IActionResult HasUpdate()
		{
			var clientIp = Request.HttpContext.Connection.RemoteIpAddress;

			return Json( this.configService.CheckUpdate( clientIp ) );
		}

		public IActionResult Config()
		{
			return Json( this.lightsService );
		}

		public async Task<IActionResult> PollConfig()
		{
			var config = await this.pollConfigSource.Task;

			return Json( config );
		}

		[ HttpPost ]
		public IActionResult IncreaseBrightness()
		{
			this.lightsService.IncreaseBrightness();

			return Ok();
		}

		[ HttpPost ]
		public IActionResult DecreaseBrightness()
		{
			this.lightsService.DecreaseBrightness();

			return Ok();
		}

		[ HttpPost ]
		public IActionResult SetOn( string on )
		{
			this.lightsService.On = on == "1";

			return Ok();
		}

		[ HttpPost ]
		public IActionResult SetOverride( string color )
		{
			var match = Regex.Match( color, @"[a-f0-9]{6}", RegexOptions.IgnoreCase );

			if ( !match.Success )
				return BadRequest();

			this.lightsService.ColorOverride = $"#{color}";

			return Ok();
		}

		[ HttpPost ]
		public IActionResult ClearOverride()
		{
			this.lightsService.ColorOverride = null;

			return Ok();
		}
	}
}